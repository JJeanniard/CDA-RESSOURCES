# Liens des sites

Cette section regroupera les liens des sites qui sont en rapport avec notre veille techno des sujets vus en cours.
**Merci de créer un titre de thème (de niveau 2) s'il n'existe pas déjà**

* Créer ensuite un titre de niveau 3 pour le sous-thème de la vidéo
* Créer un petit descriptif du site (1 ligne ou 2)
* Insérer le lien  

_Exemple dans l'onglet C#_

## C-SHARP

### Documentation Microsoft

Documentation officielle de Microsoft

* Lien : [Cliquer ici][site] !

[site]: https://docs.microsoft.com/fr-fr/dotnet/csharp/

## JS

## JAVA

## SQL

## UML

## GIT

### Cheatsheet

Attention pas d'installation de git-flow, il est déjà incorporé dans git.

* lien : [Clique ici][flowCheat] !

[flowCheat]: http://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html
