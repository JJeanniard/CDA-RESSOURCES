# Liens des vidéos
Cette section regroupera les liens des vidéos enregistrees pendant les cours avec Mickael
**Merci de créer un titre de thème (de niveau 2) s'il n'existe pas déjà**
* Créer ensuite un titre de niveau 3 pour le sous-thème de la vidéo
* Créer un petit descriptif de la vidéo (1 ligne ou 2)
* Date de la video
* Insérer le lien

Exemple dans l'onglet UML

## C-SHARP

## UML
### Introduction a l'UML :
Videos sur la prise en main de Visual Paradigm et introduction a l'UML  
**Date : 10 Juin 2020**
* Video : [Télécharger ici][site] !

[site]: http://portfolio.schoenmaeker.com/work/Mickael/2020-06-10%2009-10-11.mkv

### Cours sur l'heritage :
### Cours sur les associations :
### Cours sur les agregations et compositions :
### Cours sur les dependances :

## JAVA

## SQL
